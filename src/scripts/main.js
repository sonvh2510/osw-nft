import Plyr from 'plyr';

const initVideoDynamic = (DOM) => {
  if (DOM) {
    const src = DOM.getAttribute('src');
    const provider = src.includes('youtube')
      ? 'youtube'
      : src.includes('vimeo')
      ? 'vimeo'
      : 'default';
    const opts = {
      type: 'video',
      sources: [
        {
          src,
          provider,
        },
      ],
    };
    if (provider === 'default') {
      opts.sources[0]['type'] = 'video/mp4';
      delete opts.sources[0].provider;
      const video = document.createElement('video');
      video.muted = true;
      video.autoplay = true;
      video.playsInline = true;
      video.controls = false;
      video.src = src;
      video.loop = true;
      video.muted = true;
      video.setAttribute('muted', 'muted');
      DOM.append(video);
      const vid = new Plyr(video, { ...opts });
    } else {
      DOM.setAttribute('data-plyr-provider', provider);
      DOM.setAttribute('data-plyr-embed-id', src);
      const vid = new Plyr(DOM, { ...opts });
    }
  }
};
document.fonts.ready.then(function() {
  const activeRoadmapSliderActive = document.querySelector(
    '.swiper__roadmap-progress'
  )
    ? Number(
        document
          .querySelector('.swiper__roadmap-progress')
          .getAttribute('data-slide-active')
      )
    : 0;
  const roadmapTimeSlider = new Swiper('.osw-roadmap .swiper__roadmap-time', {
    slidesPerView: 1,
    centeredSlides: true,
    simulateTouch: false,
    allowTouchMove: false,
    initialSlide: activeRoadmapSliderActive,
    speed: 800,
    effect: 'fade',
    fadeEffect: {
      crossFade: true,
    },
    navigation: {
      nextEl: '.osw-roadmap .swiper__roadmap-navigation--next',
      prevEl: '.osw-roadmap .swiper__roadmap-navigation--prev',
    },
  });
  const roadmapProgressSlider = new Swiper(
    '.osw-roadmap .swiper__roadmap-progress',
    {
      slidesPerView: 2,
      centeredSlides: true,
      spaceBetween: 10,
      initialSlide: activeRoadmapSliderActive,
      speed: 800,
      breakpoints: {
        576: {
          slidesPerView: 3,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
        1025: {
          slidesPerView: 3,
          spaceBetween: 35,
        },
        1200: {
          slidesPerView: 3,
          spaceBetween: 64,
        },
      },
    }
  );
  roadmapTimeSlider.controller.control = roadmapProgressSlider;
  roadmapProgressSlider.controller.control = roadmapTimeSlider;
  let max = 0;
  document.querySelectorAll('.roadmap-item').forEach((i) => {
    if (i.offsetHeight > max) {
      max = i.offsetHeight;
    }
  });
  document.querySelectorAll('.roadmap-item').forEach((i) => {
    i.style.height = `${max}px`;
  });
});
document.addEventListener('DOMContentLoaded', () => {
  const headerSelect = document.querySelector('.header__language select');
  if (headerSelect) {
    headerSelect.addEventListener('change', function () {
      const { value } = this;
      const url = `/switch-language?culture=${value}`;
      function reqListener() {
        window.location.reload();
      }
      var request = new XMLHttpRequest();
      request.setRequestHeader(
        'header',
        '"X-Requested-With": "XMLHttpRequest"'
      );
      request.addEventListener('load', reqListener);
      request.addEventListener('error', (e) => {
        alert(e);
      });
      request.open('GET', url);
      request.send();
    });
  }
  const pageVerify = document.getElementById('page-verify');
  pageVerify && document.body.classList.add(pageVerify.value);

  const headerBtnToggleNav = document.querySelector('.header__nav__btn-toggle');
  const headerOverlay = document.querySelector('.header__nav__overlay');
  const headerNav = document.querySelector('.header__nav');
  headerBtnToggleNav &&
    headerBtnToggleNav.addEventListener('click', () => {
      headerNav && headerNav.classList.toggle('is-showed');
      headerOverlay && headerOverlay.classList.toggle('is-showed');
    });
  headerOverlay &&
    headerOverlay.addEventListener('click', (e) => {
      headerNav && headerNav.classList.remove('is-showed');
      headerOverlay && headerOverlay.classList.remove('is-showed');
    });

  const bannerVideoDom = document.querySelector('#osw__plyr-video');
  if (bannerVideoDom) {
    const bannerVid = bannerVideoDom.querySelector('video source');
    const src = bannerVid.src;
    const bannerVidWrapper = document.querySelector('.osw-banner__video');
    bannerVidWrapper.innerHTML = `<div id="osw__plyr-video" src="${src}"></div>`;
    initVideoDynamic(bannerVidWrapper.querySelector('#osw__plyr-video'));
  }
  const introduceVideoDOM = document.querySelector(
    '#osw-introduce__plyr-video'
  );
  initVideoDynamic(introduceVideoDOM);
  const tabVideoDom = document.querySelector('#osw-introduce__plyr-tab-video');
  initVideoDynamic(tabVideoDom);

  new Swiper('.osw-partners .swiper-container', {
    slidesPerView: 2,
    centeredSlides: true,
    spaceBetween: 10,
    loop: true,
    navigation: {
      nextEl: '.osw-partners .swiper__partners-navigation--next',
      prevEl: '.osw-partners .swiper__partners-navigation--prev',
    },
    breakpoints: {
      576: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 30,
      },
      1025: {
        slidesPerView: 4,
        spaceBetween: 35,
      },
      1200: {
        slidesPerView: 5,
        spaceBetween: 64,
      },
    },
  });

  const colors = {
    0: 'rgb(133, 92, 248, 1)',
    1: 'rgb(133, 92, 248, 0.8)',
    2: 'rgb(133, 92, 248, 0.6)',
    3: 'rgb(133, 92, 248, 0.4)',
  };
  const cards = document.querySelectorAll('.osw-card');
  Array.from(cards).forEach((card) => {
    const chartItems = card.querySelectorAll('.chart-item');
    const chartItemAfterSort = Array.from(chartItems).sort((prev, current) => {
      const prevProgress = prev.querySelector('.chart-item__progress [data-ratio-percentage]');
      const prevPercentage = prevProgress.getAttribute('data-ratio-percentage');
      const currentProgress = current.querySelector(
        '.chart-item__progress [data-ratio-percentage]'
      );
      const currentPercentage = currentProgress.getAttribute(
        'data-ratio-percentage'
      );
      return Number(currentPercentage) - Number(prevPercentage);
    });
    chartItemAfterSort.forEach((c, index) => {
      const progress = c.querySelector('.chart-item__progress [data-ratio-percentage]');
      if (progress) {
        const percentage = progress.getAttribute('data-ratio-percentage');
        const span = document.createElement('span');
        progress.append(span);
        span.textContent = `${percentage}%`;
        progress.style.width = `${percentage}%`;
        progress.style.backgroundColor = `${colors[index]}`;
      }
    });
  });
});
